# INTRODUCTION
Application assumes, there are some 'Orders' (Static in Database - MySQL) can be of any type*, these orders needs to be dispatched to the customers. \
Additionally, there has to be some notification that needs to be pushed to the customer. 
To send, these notifications (via SMS) TWILIO SMS and DELIVERY NOTIFICATION, is being utilised.

---

#### NOTE:

Application code, places in static customer receiver customer number
and also static sender contact number.
---

# CODE STRUCTURE 
Application is basic Flask app, using SQLALCHEMY ORM to interact with the MySQL DB. 

```python
app.py  # base code for the application
templates/index.html # root page view
static/app.js # comprises of ajax logic for auto refresh 
requirements.txt # all dependencies for the code
```

# APPLICATION LOGIC 

 - (Dispatch Orders)/(Push Notification) -> TWILIO SMS
[Flask APP]

 - (Twilio Delivery notification) -> GCP Cloud Function -> GCP PubSub

 - (GCP PubSub) <=PULL -> Background Process -> UpdateDB

 - (Flask APP) --Checks DB---> MySQL


# Application Execution

```bash
export DB_USER=<Database User Name>
export DB_PASS=<Database Password>
export APP_ID=<Twilio App Id>
export TOKEN=<Token for Twilio>
export CALLBACK_URI=<uri to handle notif from Twilio>
flask run
```

# Testing 
NULL
