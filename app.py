from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from twilio.rest import Client
from sqlalchemy import text
from flask_bootstrap import Bootstrap
from datetime import datetime

import os

app = Flask(__name__, template_folder="templates")
con_str = os.environ['DB_USER']+":"+os.environ['DB_PASS']+"@34.87.229.227/sample_orders"
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+con_str
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

Bootstrap(app)

account_sid = os.environ['ACC_ID']
auth_token = os.environ['TOKEN']


class Orders(db.Model):
    order_id = db.Column(db.Integer, primary_key=True)
    order_date = db.Column(db.DateTime)
    sms_id = db.Column(db.String(250))
    notification_stat = db.Column(db.JSON)
    order_status = db.Column(db.String(100))
    to_reload = db.Column(db.Integer)

    def __init__(self, order_id, order_date, order_status,
                 sms_id, notification_stat):
        self.order_id = order_id
        self.order_date = order_date,
        self.order_staus = order_status,
        self.sms_id = sms_id,
        self.notification_stat = notification_stat

@app.route("/")
def hello_world():
    orders = Orders.query.all()
    sys_curr_time = datetime.now()
    return render_template("index.html", orders=orders);


@app.route("/despatch", methods=['POST'])
def despatch_order():
    order_id = request.form['order_id']
    my_data = Orders.query.get(order_id)

    from_ph = '+1xxxxxxxxxxx'
    to_ph = '+61xxxxxxxxxxx'
    client = Client(account_sid, auth_token)
    message = client.messages.create(
        body='Something to be send .....',
        from_=from_ph,
        status_callback=os.environ['CALLBACK_URI'],
        to=to_ph
    )

    msg_id = message.sid
    print("The message id is ", msg_id)
    my_data.order_status = "DESPATCHED"
    my_data.sms_id = msg_id
    db.session.commit()
    return "success"


@app.route("/notifications",methods=['GET', 'POST'])

def notification():
    stmt = "SELECT MAX(to_reload) as to_reload from orders limit 1"
    sql = text(stmt)
    result = db.engine.execute(sql)
    res = [x[0] for x in result][0]

    if res is not None:
        u_sql = "UPDATE orders SET to_reload = NULL where to_reload = 1"
        exec_u_sql = text(u_sql)
        db.engine.execute(exec_u_sql)
        db.session.commit()
        return "True"
    else:
        return "False"

if __name__ == "__main__":
    app.run(debug=True)
